from django.http.response import Http404
from django.shortcuts import render
from rest_framework import serializers

# Create your views here.
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework.permissions import IsAuthenticated

from .serializers import VehicleAccountSerializer, VehicleDetailViewSerializer
from .models import VehicleAccount

class VehicleAccountView(APIView):
    permission_classes = (IsAuthenticated,)
    serializers_class = VehicleAccountSerializer
    def get(self,request,format=None):
        vehicles = VehicleAccount.objects.all()
        serializer = VehicleAccountSerializer(vehicles,many=True)
        return Response(serializer.data)

class VehicleDetailView(APIView):
    permission_classes = (IsAuthenticated,)
    serializers_class = VehicleDetailViewSerializer

    def get_vehicle(self,pk):
        try:
            return VehicleAccount.objects.get(pk=pk)
        except VehicleAccount.DoesNotExist:
            raise Http404
    def get(self, request,pk):
        vehicle = self.get_vehicle(pk)
        serializer = VehicleDetailViewSerializer(vehicle)
        return Response(serializer.data)
    

        