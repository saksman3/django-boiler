from django.db import models

# Create your models here.
from django.db import models

# Create your models here.
class VehicleAccount(models.Model):
    vehicleID = models.CharField(max_length=255, unique=True)
    dealNumber = models.CharField(max_length=255)
    vin = models.CharField(max_length=255)
    year = models.IntegerField()
    make = models.CharField(max_length=255)
    
    