from rest_framework import fields, serializers
from .models import VehicleAccount

class VehicleAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleAccount
        fields = ('id','dealNumber')

class VehicleDetailViewSerializer(serializers.ModelSerializer):
    class Meta:
        model = VehicleAccount
        fields = ("__all__")