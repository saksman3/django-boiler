#rest framework views
from rest_framework_simplejwt.views import TokenRefreshView
from rest_framework_simplejwt.views import TokenObtainPairView

#django views
from django.urls import path

#app views
from .views import VehicleAccountView, VehicleDetailView

urlpatterns = [
    path("token",TokenObtainPairView.as_view(),name="token_obtain_pair"),
    path("token-refresh",TokenRefreshView.as_view(),name="token_refresh"),
    path("vehicles/",VehicleAccountView.as_view(),name="vehicle_list"),
    path("vehicles/<str:pk>/",VehicleDetailView.as_view(), name="vehicle_detail"),
]